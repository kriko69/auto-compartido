import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NetworkInterface } from '@ionic-native/network-interface';
import { Observable } from "rxjs";
import { Geolocation } from '@ionic-native/geolocation';
import { ToastController } from "ionic-angular";
import { mysqlService } from "./mysql.service";


@Injectable()
export class ips{

  ipConexion:string;
  ipCelular:string;
  constructor(public mysql:mysqlService,private networkInterface: NetworkInterface,public geolocation: Geolocation,private toast:ToastController){

  }
  obtenerIP(accion,ci)
  {
    //En el construccctor poner:
    //private networkInterface: NetworkInterface ==>
    //E importar networkinterface import { NetworkInterface } from '@ionic-native/network-interface';
    this.ipConexion="";
    this.ipCelular="";
    let err;
    this.networkInterface.getWiFiIPAddress()
      .then(address => this.ipConexion = ""+address.ip)
      .catch(error => err=error);
  
    this.networkInterface.getCarrierIPAddress()
      .then(address1 => this.ipCelular = ""+address1.ip)
      .catch(error => err=error);
  setTimeout(() => {
    if(this.ipConexion != ""){
      this.ubicacion(accion,ci,this.ipConexion);
    }
    else{
      this.ubicacion(accion,ci,this.ipCelular);
    }
  }, 2000);
  }
  ubicacion(accion,ci,ip){
    let fecha=this.dameFecha();
    this.geolocation.getCurrentPosition().then((result) => {
      let latOri = result.coords.latitude;
      let longOri = result.coords.longitude;
      console.log("lat:",latOri);
      console.log("long:",longOri);
      console.log("ip:",ip);
      console.log("accion:",accion); 
      console.log("fecha:",fecha);
      this.subir(ci,latOri,longOri,ip,accion,fecha);   
    }).catch(function(e) {
      console.log('2-error')
      alert('GPS desativado. Verifique que este activo y que la aplicacion tenga permiso de usarlo(Configuración>Aplicaciones>Aventón>Permisos)!')    });
  }
  subir(ci,lat,long,ip,accion,fecha){
    this.mysql.agregaracciones(ci,lat,long,ip,accion,fecha).subscribe(
      data=>{
        console.log('respuesta',data);
      },(error)=>{
        console.log('respuesta',error);
      }
    );
  }
  dameFecha()
  {
    let hoy = new Date();
    let dd = hoy.getDate();
    let mm = hoy.getMonth()+1;
    let yyyy = hoy.getFullYear();
    let hora=''+hoy.getHours();
    let minutos=''+hoy.getMinutes();
    let segundos=''+hoy.getSeconds();
    if(hoy.getHours()<10)
      hora='0'+hora;
    if(hoy.getMinutes()<10)
      minutos='0'+minutos;
    if(hoy.getSeconds()<10)
      segundos='0'+segundos;
    let date=yyyy+'-'+mm+'-'+dd+' '+hora+':'+minutos+':'+segundos;

    return date;
  }
}
