import { Usuario } from './../../interfaces/usuario.interface';
import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { mysqlService } from '../../services/mysql.service';

@Injectable()
export class UbicacionService {
  usuario={alat:0,along:0};
  watchId;
  watchId1;
  bool=true;
  constructor(private geolocation:Geolocation,public mysql:mysqlService) {
    console.log('Hello UbicacionProvider Provider');
  }
  iniciar_localizacion(id_viaje,val){
  if(this.bool){
    this.ubicacion(id_viaje);
    this.bool=false;
  }
this.watchId = navigator.geolocation.watchPosition((data) => {
  this.usuario.alat=data.coords.latitude;
  this.usuario.along=data.coords.longitude;
  setTimeout(() => {
  this.mysql.actualizarLocaclizacion(this.usuario.alat,this.usuario.along,id_viaje).subscribe(
    data=>{
      console.log('data:',data);

    },(error)=>{
      console.log(error);

    }
  );
  }, val);
})
  }
  cortar_localizacion(){
    navigator.geolocation.clearWatch(this.watchId);
  }

  ubicacion(id_viaje){
    this.geolocation.getCurrentPosition().then((result) => {
      let latOri = result.coords.latitude;
      let longOri = result.coords.longitude;
      console.log("lat:",latOri);
      console.log("long:",longOri); 
      this.mysql.actualizarLocaclizacion(this.usuario.alat,this.usuario.along,id_viaje).subscribe(
        data=>{
          console.log('data:',data);
    
        },(error)=>{
          console.log(error);
    
        }
      );
    }).catch(function(e) {
      console.log('2-error')
      alert('GPS desativado. Verifique que este activo y que la aplicacion tenga permiso de usarlo(Configuración>Aplicaciones>Aventón>Permisos)!')    });
  }
  obtnTiempo(id_viaje){
    let vec=[];
    let val=10;
    let info;
    this.mysql.obtenerConfAdmin().subscribe(
      data=>{
        console.log('data:',data);
        info=Object.assign(data);

      },(error)=>{
        console.log(error);

      }
    );
    setTimeout(()=>{
      if(info != undefined)
      {
        if(info['message']!="ERROR")
        {
          vec=info;
          val=info['tiempo_subida_ubicacion'];
          this.iniciar_localizacion(id_viaje,val);
        }
        else{
          this.iniciar_localizacion(id_viaje,val);
        }
      }
      else{
        this.iniciar_localizacion(id_viaje,val);
      }
    },2000);

  }
}
