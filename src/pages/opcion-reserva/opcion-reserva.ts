import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { ReservaPasajeroPage } from '../reserva-pasajero/reserva-pasajero';
import { mysqlService } from '../../services/mysql.service';

/**
 * Generated class for the OpcionReservaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-opcion-reserva',
  templateUrl: 'opcion-reserva.html',
})
export class OpcionReservaPage {

  id_usuario;
  reserva;
  ruta={
    capacidad:0
  }
  info=[];
  auxi;
  onj={
    placa:'',
    nombre:'',
    apellido:'',
    telf:0
  };
  base64Image: string='';
  fotoAuto: string;
  fotoAuto1:string;
  aux;
  constructor(public navCtrl: NavController, public navParams: NavParams,private platform:Platform,public mysql:mysqlService) {
    this.platform.registerBackButtonAction(() => {
      console.log('');
    },10000);

    this.id_usuario=this.navParams.get('id_usuario');
    this.reserva=this.navParams.get('reserva');
    console.log('reserva',this.reserva);
    this.func();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OpcionReservaPage');
    this.fotoAuto = this.reserva.id_para;
    this.mysql.validarFotoUsuario(this.fotoAuto).subscribe(
      data=>{
        if(data['message']=="existe")
        {
          this.fotoAuto = "http://ucbtec.net/Aventon/img/Perfil/"+this.fotoAuto + ".jpg?random+\="+this.aux;
        }
        if(data['message']=="no existe")
        {
          this.fotoAuto = "http://ucbtec.net/Aventon/img/defaultUsuario.jpg";
        }
        this.base64Image=this.fotoAuto;
      },error=>{
        
      }
      
    );
  }
  func2(){
    
    this.fotoAuto1 = this.onj.placa;
    console.log("FOTO ");
      this.mysql.validarFotoAuto(this.onj.placa).subscribe(
        data=>{
          console.log(data);
          if(data['message']=="existe")
          {
            console.log("FOTO VALIDADA");
            this.fotoAuto1 = "http://ucbtec.net/Aventon/img/Autos/"+this.fotoAuto1 + ".jpg?random+\="+this.aux;//data[placa] tiene que ser devuelta de la consulta
          }
          if(data['message']=="no existe")
          {
            console.log("FOTO  NO VALIDADA");
            this.fotoAuto1 = "http://ucbtec.net/Aventon/img/defaultAuto.jpg";
          }
          this.base64Image=this.fotoAuto1;
        },error=>{ 
        }
      );
  }
  func(){
    let info;
    this.mysql.obtnnumplaca(this.reserva.id_viaje,this.reserva.id_para).subscribe(
      data => {
        console.log('data', data);
        info= Object.assign(data);
        console.log('exito');
        this.auxi=info;


        }, (error: any)=> {
          console.log('error', error);

        }
    );
    setTimeout(()=>{
      if(this.auxi!=undefined){
        if(this.auxi['message']!='ERROR'){
          this.onj.placa=this.auxi[0].placa;
          this.onj.nombre=this.auxi[0].nombre;
          this.onj.apellido=this.auxi[0].apellido;
          this.onj.telf=this.auxi[0].telf;
          this.func2();
        }
      }

    },1000);
  }

  eliminarReserva()
  {
    let info;
    let capacidad;
    this.mysql.eliminarSolicitud(this.reserva.id_solicitud).subscribe(
      data => {
        console.log('data', data);
        info= Object.assign(data);
        console.log('exito');


        }, (error: any)=> {
          console.log('error', error);

        }
    );
    setTimeout(()=>{
      this.mysql.obtenerCapacidadViaje(this.reserva.id_viaje).subscribe(
        data=>{
          console.log(data);
          capacidad=data;
        },(error)=>{
          console.log(error);
  
        }
      );
      setTimeout(()=>{
        capacidad=Number(capacidad)+1;
        this.mysql.actualizarCapacidad(capacidad,this.reserva.id_viaje).subscribe(
          data=>{
            console.log(data);
          },(error)=>{
            console.log(error);
  
          }
        );
        setTimeout(()=>{

          this.navCtrl.setRoot(ReservaPasajeroPage,{id_usuario:this.id_usuario/*,id_auto:this.id_auto*/});

        });
  
      },1000);

    },1000);
  }

}
