import { mysqlService } from './../../services/mysql.service';
import { ActivarRutaPage } from './../activar-ruta/activar-ruta';
import { ToastService } from './../../services/toast.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController, AlertController, ToastController, Platform, App, LoadingController } from 'ionic-angular';
import {RutaModel} from '../../shared/ruta-model';
import {AddRutaPage} from '../add-ruta/add-ruta';
import { AddrutaproPage } from '../addrutapro/addrutapro';
import { rutaprogramada } from '../../interfaces/ruta.programada.interface';
/**
 * Generated class for the ViajePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
import { ISubscription } from 'rxjs/Subscription';
import { VerMiRutaPage } from '../ver-mi-ruta/ver-mi-ruta';
import { ips } from '../../services/ips';
import { isPresent } from 'ionic-angular/umd/util/util';
import { InfoRutaConductorPage } from '../info-ruta-conductor/info-ruta-conductor';

@IonicPage()
@Component({
  selector: 'page-viaje',
  templateUrl: 'viaje.html',
})
export class ViajePage {
  email='';
  vec:any=[];
  cacpacidad=0;
  placa;
  control1:ISubscription;

  //buscardor
  fecha;
  resultados=[];
  copia=[];
  public viajes:RutaModel[];



  id_usuario;
  id_auto;
  nombres=[];
  value=false;
  programadas=[];
  ip;
  constructor(public app:App,public navCtrl: NavController,
    public alertCtrl:AlertController, public toast:ToastService,public navParams: NavParams,
    public modalCtrl:ModalController,public mysql:mysqlService,
    public viewCtrl:ViewController,private platform:Platform,public ips:ips, public load:LoadingController) {
      this.platform.registerBackButtonAction(() => {
        console.log('');
      },10000);

      this.id_auto=this.navParams.get('id_auto');
      this.id_usuario=this.navParams.get('id_usuario');

      this.listarProgramadas();

      this.fecha=this.dameFecha();
      this.copia=this.vec;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViajePage');
  }

  presentLoading() {
    const loader = this.load.create({
      content: "Actualizando rutas...",
      duration: 1000
    });
    loader.present();
  }

  listarProgramadas(){
    this.presentLoading();
    this.vec=[];
          this.programadas=[];
    let info;
    this.mysql.listarProgramadas(this.id_usuario).subscribe(
      data=>{
        console.log('data:',data);
        info=Object.assign(data);

      },(error)=>{
        console.log(error);

      }
    );
    setTimeout(()=>{
      if(info != undefined)
      {
        if(info['message']!="No se encontro rutas_viaje")
        {
          this.vec=info;
          this.programadas=info;
          this.value=false;
        }
        else{
          this.value=true;
        }
      }else{
        this.value=true;
      }
    },1000);

  }

  seleccinaRuta(){
    this.navCtrl.setRoot(AddrutaproPage,{id_usuario:this.id_usuario,id_auto:this.id_auto});
  }
  // En viaje.ts luego de de la funcion seleccionaRuta()
//funcion para eliminar rutas
dameFecha2()
  {
    let hoy = new Date();
    let dd = hoy.getDate();
    let mm = hoy.getMonth()+1;
    let yyyy = hoy.getFullYear();
    let hora=''+hoy.getHours();
    let minutos=''+hoy.getMinutes();
    let segundos=''+hoy.getSeconds();
    if(hoy.getHours()<10)
      hora='0'+hora;
    if(hoy.getMinutes()<10)
      minutos='0'+minutos;
    if(hoy.getSeconds()<10)
      segundos='0'+segundos;
    let date=yyyy+'-'+mm+'-'+dd+' '+hora+':'+minutos+':'+segundos;

    return date;
  }
borrarRutaAgendada(vecs){
  let id_viaje=vecs.id_viaje;
  if(vecs.estado!='Activa')
  {
   let alert = this.alertCtrl.create({
    title: 'Confirmar borrado',
    message: '¿Estás seguro de que deseas eliminar esta ruta?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
          // Ha respondido que no así que no hacemos nada
        }
      },
      {
        text: 'Si',
        handler: () => {
          this.ips.obtenerIP("Cancelar Viaje",this.id_usuario);
          this.Notificar(id_viaje);
         }
      }
    ]
  });

  alert.present();
}
}

dameFecha()
{
  let hoy = new Date();
  let dd = hoy.getDate();
  let mm = hoy.getMonth()+1;
  let yyyy = hoy.getFullYear();
  let mes,dia;
  if(mm<10)
  {
    mes='0'+mm;
  }else{
    mes=mm;
  }

  if(dd<10)
  {
    dia='0'+dd;
  }else{
    dia=dd;
  }
  let date=yyyy+'-'+mes+'-'+dia;
  return date;
}
cancelarFiltrado()
{
  this.vec=this.programadas;
  console.log(this.programadas);
  //this.fecha=this.dameFecha(); //esto no lo reconoce por el ionchange
  
}

filtrarporfecha()
{
  this.vec=this.programadas;
  this.resultados=[]; //limpio
  this.copia=this.vec;
  let aux;
  console.log(this.copia);

  for (let i = 0; i < this.copia.length; i++) {
    aux=this.copia[i].fecha_hora.split(' ');
    console.log('datetime:',this.fecha);
    console.log('mi fecha',aux[0]);


    if(aux[0]==this.fecha)
      this.resultados.push(this.copia[i]);
  }
  console.log(this.resultados);
  this.vec=this.resultados;
}
verificarFecha(fecha_hora)
{
  let divisor=fecha_hora.split(' '); //2018-02-24 17:15:00
  let fecha=divisor[0].split('-'); //2018-10-20
  let hora=divisor[1].split(':'); //17:15:00
  let hoy = new Date();
  let dd = hoy.getDate();
  let mm = hoy.getMonth()+1;
  let yyyy = hoy.getFullYear();
  let date: Date = new Date();
  let hh = date.getHours();//obtiene horas del sistema
  let min = date.getMinutes();//obtiene minutos del sistema
  if(Number(min)<=10){
    hh = hh-1;
    min = min-10+60;
  }
  else{
    min = min-10;
  }
  if((Number(fecha[0])==yyyy) && (Number(fecha[1])==mm) && ((Number(fecha[2]))==dd+1))
  {
    //console.log('falta un dia');
    return 'falta un dia';
  }
  else
  {
    if(((Number(fecha[0])<yyyy) && (Number(fecha[1])<mm) && (Number(fecha[2])<dd))
    || ((Number(fecha[0])==yyyy) && (Number(fecha[1])==mm) && (Number(fecha[2])<dd))
    || ((Number(fecha[0])==yyyy) && (Number(fecha[1])<mm))
    || ((Number(fecha[0])<yyyy))
    || ((Number(hora[0])<=hh) && Number(hora[1]<=min))
    || ((Number(hora[0])==hh) && Number(hora[1]<=min)))
    {
      //console.log('pasado');
      return 'pasado';
    }
    else
    {
      if((Number(fecha[0])==yyyy) && (Number(fecha[1])==mm) && (Number(fecha[2])==dd))
      {
        //console.log('es hoy');
        return 'hoy';
      }
      else
      {
        if((Number(fecha[0])>yyyy) || (Number(fecha[1])>mm) || (Number(fecha[2])>dd+1))
        {
          //console.log('aun falta');
          return 'falta';
        }
      }
    }
  }


}
iractiva(ruta){
  var nav = this.app.getRootNav();
    nav.setRoot(VerMiRutaPage,{id_usuario:this.id_usuario,id_auto:this.id_auto,ruta_activada:ruta});
  
}

  activarRuta(ruta)
  {
    this.navCtrl.setRoot(ActivarRutaPage,{id_usuario:this.id_usuario,id_auto:this.id_auto, ruta:ruta});
  }
  Notificar(id_viaje)
  {    
    let integrantes;
    this.mysql.listarIntegrantesPorRuta(id_viaje).subscribe(
      data=>{
        console.log('integrantes',data);
        integrantes=data;

      },(error)=>{
        console.log(error);

      }
    );
    setTimeout(()=>{
      this.fecha=this.dameFecha2();
      if(integrantes!=undefined){
        this.mysql.EliminarRutaProgramada(id_viaje).subscribe(
        data=>{
          console.log(data);
          if(data['message']=='OK')
          {
            this.toast.show('Ruta eliminada!');
            this.navCtrl.setRoot(ViajePage,{id_usuario:this.id_usuario,id_auto:this.id_auto});
          }
        },(error)=>{
          console.log(error);

        }
      );
      if(integrantes.message!="No se encontro integrantes"){
      console.log(integrantes);
        let estado='Borrada';
        let mensaje='La Ruta fue eliminada!';
        this.fecha=this.dameFecha2();
      for (let i = 0; i < integrantes.length; i++) {
        this.mysql.enviarSolicitudDeActivada(this.id_usuario,integrantes[i].ci,this.fecha,estado,mensaje,id_viaje).subscribe(
          data=>{
            console.log(data);
          },(error)=>{
            console.log(error);
          }
        );

      }
    }}
    },2000);
   /* var nav = this.app.getRootNav();
    nav.setRoot(VerMiRutaPage,{id_usuario:this.id_usuario,id_auto:this.id_auto,ruta_activada:this.ruta});
  */}
  infoRuta(vecs){
    
    this.navCtrl.push(InfoRutaConductorPage,{id_usuario:this.id_usuario,id_auto:this.id_auto,vecs:vecs});

}

}
