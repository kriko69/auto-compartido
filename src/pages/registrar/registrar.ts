import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, PopoverController } from 'ionic-angular';
import { Usuario } from '../../interfaces/usuario.interface';
import { LoginPage } from '../login/login';
import { AlertController } from 'ionic-angular';

import {FormGroup, FormBuilder, Validators} from '@angular/forms'; // Para la validacion del formulario
import { Camera, CameraOptions } from '@ionic-native/camera'; //Para la camara
/**
 * Generated class for the RegistrarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

import { mysqlService } from '../../services/mysql.service';
import { Subscription, Observable } from 'rxjs';
import { ips } from '../../services/ips';
import { Storage } from '@ionic/storage';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { TerminosPage } from '../terminos/terminos';

@IonicPage()
@Component({
  selector: 'page-registrar',
  templateUrl: 'registrar.html',
})
export class RegistrarPage {


  dataMYSQL={
    carnet:0,
    nombre:'',
    apellido:'',
    pass1:'',
    fecha_nac:'',
    tipo:'',
    telf:0,
    calif_pasa:0,
    calif_cond:0
  };

  submitAttempt: boolean = false;
  myForm: FormGroup;
ip;
  constructor(public navCtrl: NavController, public navParams: NavParams,public alerta:AlertController,private platform:Platform, private http: HttpClient
   , public mysql:mysqlService, public formBuilder: FormBuilder, public camera:Camera,public ips:ips,public storage:Storage,
   private permission: AndroidPermissions, public popoverCtrl:PopoverController) {

      this.platform.registerBackButtonAction(() => {
        console.log('');
      },10000);
    
      this.myForm = this.formBuilder.group({
        nombre: ['', Validators.compose([Validators.maxLength(20),Validators.required])],
        apellido: ['', Validators.compose([Validators.maxLength(25),Validators.required])],
        carnet: [0, Validators.compose([Validators.maxLength(15), Validators.required])],
        fecha_nac: ['', Validators.compose([ Validators.required])],
        //p_email: ['', Validators.compose([Validators.required])],
        //password_ctrl: this.formBuilder.group({
        pass1: ['', Validators.compose([Validators.pattern(/^[A-Za-z0-9]{6,}$/), Validators.required])],
        tipo:'',
        confirm_password: ['', Validators.compose([Validators.pattern(/^[A-Za-z0-9]{6,}$/), Validators.required])],
        telf: [ 0, Validators.compose([Validators.maxLength(15), Validators.required])],
        //}, this.matchPassword)
        condiciones:[,Validators.requiredTrue]
      }, {'validator': this.matchingPasswords('pass1', 'confirm_password')});

  }

  
  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    // TODO maybe use this https://github.com/yuyang041060120/ng2-validation#notequalto-1
    return (group: FormGroup): {[key: string]: any} => {
      let password = group.controls[passwordKey];
      let confirm_password = group.controls[confirmPasswordKey];

      if (password.value !== confirm_password.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }

  /*ionViewDidLoad(){
    this.servicio.getId();
  }*/


  registrar()
  {
    this.ips.obtenerIP("Registrar Usuario",this.myForm.value.carnet);
    this.dataMYSQL.nombre = this.myForm.value.nombre;
    this.dataMYSQL.apellido = this.myForm.value.apellido;
    this.dataMYSQL.carnet = this.myForm.value.carnet;
    this.dataMYSQL.fecha_nac = this.myForm.value.fecha_nac;
    this.dataMYSQL.pass1 = this.myForm.value.pass1;
    this.dataMYSQL.telf = this.myForm.value.telf;
    console.log(this.dataMYSQL);
    let info={};
    
    this.mysql.AgregarUsuario(this.dataMYSQL).subscribe(
      data => {
        console.log('data', data);
        info= Object.assign(data);
        console.log('exito');


        }, (error: any)=> {
          console.log('error', error);
          console.log();

        }
    );

    setTimeout(()=>{
      console.log('info',info);
      if(info!=undefined)
      {
        /*if(info['message']=='OK')
        {*/
          this.mostrarAlerta();
          this.navCtrl.push(LoginPage);
       /* }
      else
        this.mostrarAlerta2();*/
      }
      else{
        this.mostrarAlerta2();
      }
        

    },1000);


  }

  mostrarAlerta() {
    const alert = this.alerta.create({
      title: 'Registrado exitoso!',
      subTitle: 'Ahora ya puedes ingresar a la aplicacion',
      buttons: ['OK']
    });
    alert.present();
  }
    
  mostrarAlerta2() {
    const alert = this.alerta.create({
      title: 'Registrado Fallido!',
      subTitle: 'Vuelva a intentar por favor.',
      buttons: ['OK']
    });
    alert.present();
  }
  base64Image: any='../assets/defaultUsuario.jpg';
  openCamera(){
    this.permission.checkPermission(this.permission.PERMISSION.CAMERA).then(
      result => console.log('tiene permiso?',result.hasPermission),
      err => this.permission.requestPermission(this.permission.PERMISSION.CAMERA)
    );
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation:true
    }

    this.camera.getPicture(options).then((imageData)=> {
    this.base64Image = 'data:image/jpeg;base64,'+ imageData;
  },(err)=>{
    console.log('Error en la foto tomada')
  });
  
  }

  openGallery(){
    this.permission.checkPermission(this.permission.PERMISSION.CAMERA).then(
      result => console.log('tiene permiso?',result.hasPermission),
      err => this.permission.requestPermission(this.permission.PERMISSION.CAMERA)
    );
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation:true
    }

    this.camera.getPicture(options).then((imageData)=> {
    this.base64Image = 'data:image/jpeg;base64,'+ imageData;
  },(err)=>{
    console.log('Error en la foto tomada')
  });
  }

  uploadingFoto(){
    this.storage.set('foto', 1000);
    let url = 'http://ucbtec.net/Aventon/img/Perfil/subirfotosperfil.php';
    let postData = new FormData();
    let nombre = this.myForm.value.carnet;
    postData.append('file',this.base64Image);
    postData.append('nombre',nombre)
    let data: Observable<any> = this.http.post(url,postData);
    data.subscribe((res)=>{
      console.log(res);
    });
  }
  dameFecha()
  {
    let hoy = new Date();
    let dd = hoy.getDate();
    let mm = hoy.getMonth()+1;
    let yyyy = hoy.getFullYear();
    let date=yyyy+'-'+mm+'-'+dd;

    return date;
  }

  validarCi()
  {
    let ci = this.myForm.value.carnet;
    this.mysql.validarCi(ci).subscribe(
      data=>{
        console.log(data);
        if(data['message']=="existe")
        {
          this.mysql.validarExistenciaCi(ci).subscribe(
          data=>{
            console.log(data);
            if(data['message']=="existe")
            {
              this.mostrarAlerta4(); 
            }
            if(data['message']=="no existe")
            {
              this.registrar();
            }
          },error=>{       
          }
          
        );
        }
        if(data['message']=="no existe")
        {
          this.mostrarAlerta3();
        }
      },error=>{
        this.mostrarAlerta3();        
      }
      
    );
  }
  mostrarAlerta3() {
    const alert = this.alerta.create({
      title: 'Registrado Fallido!',
      subTitle: 'El carnet ingresado no es de un estudiante de la UCB.',
      buttons: ['OK']
    });
    alert.present();
  }

 
  mostrarAlerta4() {
    const alert = this.alerta.create({
      title: 'Registrado Fallido!',
      subTitle: 'El carnet ingresado ya está registrado.',
      buttons: ['OK']
    });
    alert.present();
  }
  irTerminos() {
    this.navCtrl.push(TerminosPage);
  }
}
