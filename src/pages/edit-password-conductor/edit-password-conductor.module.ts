import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditPasswordConductorPage } from './edit-password-conductor';

@NgModule({
  declarations: [
    EditPasswordConductorPage,
  ],
  imports: [
    IonicPageModule.forChild(EditPasswordConductorPage),
  ],
})
export class EditPasswordConductorPageModule {}
