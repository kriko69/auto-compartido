import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingCmp, LoadingController } from 'ionic-angular';
import { ReservaPasajeroPage } from '../reserva-pasajero/reserva-pasajero';
import { mysqlService } from '../../services/mysql.service';

/**
 * Generated class for the InfoRutaConductorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-info-ruta-conductor',
  templateUrl: 'info-ruta-conductor.html',
})
export class InfoRutaConductorPage {

  id_usuario;
  id_auto;
  vecs;
  obj=[];
  aux2={
    nombre:'',
    apellido:'',
    telf:'',
    fecha:'',
    foto:''
  };

  auxi;
  base64Image;
  fotoAuto: string;
  aux;
  constructor(public load:LoadingController,public navCtrl: NavController, public navParams: NavParams,private platform:Platform,public mysql:mysqlService) {
    this.platform.registerBackButtonAction(() => {
      console.log('');
    },10000);

    
    this.id_auto=this.navParams.get('id_auto');
    this.id_usuario=this.navParams.get('id_usuario');
    this.vecs=this.navParams.get('vecs');
    this.func();
    this.presentLoading();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OpcionReservaPage');
  }
  func(){
    let info;
    this.mysql.listarIntegrantesTELF(this.vecs.id_viaje).subscribe(
      data => {
        console.log('data', data);
        info= Object.assign(data);
        console.log('exito');
        this.auxi=info;


        }, (error: any)=> {
          console.log('error', error);

        }
    );
    setTimeout(()=>{
      console.log('auxi',this.auxi);
      let j=0;
      if(this.auxi!=undefined){
        if(this.auxi['message']!='ERROR'){
          for(let i=0;i<this.auxi.length;i++)
          {
            this.fotoAuto = this.auxi[j].ci;
            console.log('fotoauto',this.auxi[i].ci);
            this.mysql.validarFotoUsuario(this.fotoAuto).subscribe(
              data=>{
                if(data['message']=="existe")
                {
                  this.fotoAuto = "http://ucbtec.net/Aventon/img/Perfil/"+this.fotoAuto + ".jpg?random+\="+this.aux;
                  this.auxi[i].foto=this.fotoAuto;
                }
                else
                {
                  this.fotoAuto = "http://ucbtec.net/Aventon/img/defaultUsuario.jpg";
              this.auxi[i].foto=this.fotoAuto;
                }
              },error=>{
              }
              
            );
            setTimeout(() => {
            }, 1000);
          }
          
          console.log('obj',this.obj);
        }
      }
    },2000);
  }
  presentLoading() {
    const loader = this.load.create({
      content: "Espere por favor...",
      duration: 2000
    });
    loader.present();
  }

}
