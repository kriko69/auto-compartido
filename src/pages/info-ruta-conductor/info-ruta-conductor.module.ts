import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfoRutaConductorPage } from './info-ruta-conductor';

@NgModule({
  declarations: [
    InfoRutaConductorPage,
  ],
  imports: [
    IonicPageModule.forChild(InfoRutaConductorPage),
  ],
})
export class InfoRutaConductorPageModule {}
