import { mysqlService } from './../../services/mysql.service';
import { ViajePage } from './../viaje/viaje';
import { rutaprogramada } from './../../interfaces/ruta.programada.interface';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform, LoadingController } from 'ionic-angular';
import { Ruta } from '../../interfaces/rutas.interface';
import { ISubscription } from 'rxjs/Subscription';
import { ToastService } from '../../services/toast.service';
import { MarkadorPage } from '../markador/markador';
import { ips } from '../../services/ips';

/**
 * Generated class for the AddrutaproPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addrutapro',
  templateUrl: 'addrutapro.html',
})
export class AddrutaproPage {
  isenabled=false;
  email='';
  aux:any;
  control1:ISubscription;
  fechahora;
  vec:any;
  placa;
  paga=true;
  ruta;
  id_usuario;
  id_auto;
  capacidad;
  fechaActual;
  ip;
  constructor(public navCtrl: NavController, public navParams: NavParams,public mysql:mysqlService,
    public viewCtrl:ViewController,public toast: ToastService,private platform:Platform,public ips:ips,
    public load:LoadingController) {
      this.platform.registerBackButtonAction(() => {
        console.log('');
      },10000);
      this.id_auto=this.navParams.get('id_auto');
      this.id_usuario=this.navParams.get('id_usuario');
      console.log(this.dameFecha());
      this.fechahora=this.dameFecha();
      this.fechaActual=this.dameFechaPlaceHolder();
  }

  ionViewDidLoad() {
    this.func();
  }
  presentLoading() {
    const loader = this.load.create({
      content: "Buscando rutas...",
      duration: 1000
    });
    loader.present();
  }
  func(){
    this.presentLoading();
    let info;
    this.mysql.listarRutasParaProgramar(this.id_usuario).subscribe(
      data=>{
        console.log('nombre rutas: ',data);
        info=data;
        console.log('vec',this.vec);

      },(error)=>{
        console.log(error);

      }
    );setTimeout(() => {
      if(info!=undefined)
      {
        if(info['message']!='No se encontro rutas con este ci')
        {
          this.vec=info;
        }
        else{
          this.vec=[];
        }
      }
      else{
        this.vec=[];
      }

    }, 1000);
  }
  submit(){
    this.presentLoading2();
    this.ips.obtenerIP("Agendar Viaje",this.id_usuario);
    let ruta=this.ruta;
    let divisor1=this.fechahora.split('T');
    let fecha=divisor1[0];
    let divisor2=divisor1[1].split('Z');
    let hora=divisor2[0];
    this.mysql.obtenerCapacidadAuto(this.id_auto).subscribe(
      data=>{
        console.log(data);
        this.capacidad=data;
      },(error)=>{
        console.log(error);

      }
    );
    setTimeout(()=>{
      let programada={
        'id_auto':Number(this.id_auto),
        'capacidad':Number(this.capacidad),
        'fecha_hora':fecha+' '+hora,
        'id_ruta':Number(ruta.id_ruta),
        'id_conductor':Number(this.id_usuario)
      };
      console.log(programada);
      this.mysql.agregarRutaProgramada(programada).subscribe(
        data=>{
          console.log(data);
          if(data['message']=='OK')
          {
            this.toast.show(` Ruta agendada!`);
            this.navCtrl.setRoot(ViajePage,{id_usuario:this.id_usuario,id_auto:this.id_auto});
          }
        },(error)=>{
          console.log(error);

        }
      );

    },1000);
  }
  dismiss(){
    this.navCtrl.setRoot(ViajePage,{id_usuario:this.id_usuario,id_auto:this.id_auto});
  }

  dameFecha()
  {
    let hoy = new Date();
    let dd = hoy.getDate();
    let mm = hoy.getMonth()+1;
    let yyyy = hoy.getFullYear();
    let hora=''+hoy.getHours();
    let minutos=''+hoy.getMinutes();
    let segundos=''+hoy.getSeconds();
    if(hoy.getHours()<10)
      hora='0'+hora;
    if(hoy.getMinutes()<10)
      minutos='0'+minutos;
    if(hoy.getSeconds()<10)
      segundos='0'+segundos;
    //let date=dd+'-'+mm+'-'+yyyy+'T'+hora+':'+minutos+':'+segundos+'Z';
    let date=yyyy+'-'+mm+'-'+dd+'T'+hora+':'+minutos+':'+segundos+'Z';

    return date;
  }

  dameFechaPlaceHolder()
  {
    let hoy = new Date();
    let dd = hoy.getDate();
    let mm = hoy.getMonth()+1;
    let yyyy = hoy.getFullYear();
    let hora=''+hoy.getHours();
    let minutos=''+hoy.getMinutes();
    let segundos=''+hoy.getSeconds();
    if(hoy.getHours()<10)
      hora='0'+hora;
    if(hoy.getMinutes()<10)
      minutos='0'+minutos;
    if(hoy.getSeconds()<10)
      segundos='0'+segundos;
    let date=dd+'/'+mm+'/'+yyyy+' '+hora+':'+minutos;
    return date;
  }
  valor(vecs){
    console.log("Vecs",vecs);
    this.isenabled=true;
    this.ruta=vecs;
  }
  NuevaRuta(){
    this.navCtrl.setRoot(MarkadorPage,{id_usuario:this.id_usuario,id_auto:this.id_auto,pageanterior:'Addrutapro'});
  }
  presentLoading2() {
    const loader = this.load.create({
      content: "Espere por favor...",
      duration: 2000
    });
    loader.present();
  }
}
