import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReportedeproblemapasajeroPage } from './reportedeproblemapasajero';

@NgModule({
  declarations: [
    ReportedeproblemapasajeroPage,
  ],
  imports: [
    IonicPageModule.forChild(ReportedeproblemapasajeroPage),
  ],
})
export class ReportedeproblemapasajeroPageModule {}
