import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, Platform, AlertController } from 'ionic-angular';
import { mysqlService } from '../../services/mysql.service';
import { OpcionesConductorPage } from '../opciones-conductor/opciones-conductor';
import { OpcionesPasajeroPage } from '../opciones-pasajero/opciones-pasajero';
import { ips } from '../../services/ips';

/**
 * Generated class for the ReportedeproblemaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-reportedeproblemapasajero',
  templateUrl: 'reportedeproblemapasajero.html',
})
export class ReportedeproblemapasajeroPage {
  problema='';
  id_usuario;
  ip;
    constructor(public navCtrl: NavController, public navParams: NavParams,public app:App,private platform:Platform,
      public mysql:mysqlService,public alerta:AlertController,public ips:ips) {
        this.platform.registerBackButtonAction(() => {
          console.log('');
        },10000);
      this.id_usuario = navParams.get('id_usuario');
    }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad ReportedeproblemaPage');
    }
    aceptar(){
      this.ips.obtenerIP("Reservar Ruta",this.id_usuario);
      let fecha=this.dameFecha();
      this.mysql.AgregarProblema(this.id_usuario,this.problema,fecha).subscribe(
        data=>{
          console.log('respuesta',data);
        },(error)=>{
          console.log('respuesta',error);
        }
      );
      setTimeout(() => {
        this.mostrarAlerta();
        this.navCtrl.setRoot(OpcionesPasajeroPage,{id_usuario: this.id_usuario});
      }, 1000);
    }
    dismiss(){
      this.navCtrl.setRoot(OpcionesPasajeroPage,{id_usuario: this.id_usuario});
    }
    dameFecha()
    {
      let hoy = new Date();
      let dd = hoy.getDate();
      let mm = hoy.getMonth()+1;
      let yyyy = hoy.getFullYear();
      let hora=''+hoy.getHours();
      let minutos=''+hoy.getMinutes();
      let segundos=''+hoy.getSeconds();
      if(hoy.getHours()<10)
        hora='0'+hora;
      if(hoy.getMinutes()<10)
        minutos='0'+minutos;
      if(hoy.getSeconds()<10)
        segundos='0'+segundos;
      let date=yyyy+'-'+mm+'-'+dd+' '+hora+':'+minutos+':'+segundos;
  
      return date;
    }
    mostrarAlerta() {
      const alert = this.alerta.create({
        title: 'Gracias po su colaboracion!',
        subTitle: 'Reporte guardado.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  