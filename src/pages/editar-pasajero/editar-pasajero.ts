import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, LoadingController } from 'ionic-angular';
import { OpcionesPasajeroPage } from '../opciones-pasajero/opciones-pasajero';
import { Usuario } from '../../interfaces/usuario.interface';
import { ISubscription } from 'rxjs/Subscription';
import { PasajeroPage } from '../pasajero/pasajero';
import { mysqlService } from '../../services/mysql.service';
import { ToastService } from '../../services/toast.service';

import {FormGroup, FormBuilder, Validators} from '@angular/forms'; // Para la validacion del formulario
import { Camera, CameraOptions } from '@ionic-native/camera';
import { HttpClient } from '@angular/common/http';
import { Subscription, Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import { AndroidPermissions } from '@ionic-native/android-permissions';
/**
 * Generated class for the EditarPasajeroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editar-pasajero',
  templateUrl: 'editar-pasajero.html',
})
export class EditarPasajeroPage {
  usuario;
  id_usuario;
  control:ISubscription;
  fotoUsuario: string= "";
  val: boolean=false;
  base64Image: string='';
  submitAttempt: boolean = false;
  myForm: FormGroup;
  aux;


   constructor(public navCtrl: NavController, public navParams: NavParams, 
    public alerta:AlertController, private platform:Platform,
    public mysql:mysqlService,public load:LoadingController,public toast:ToastService, public camera:Camera,
    private http: HttpClient,public formBuilder: FormBuilder, public storage:Storage, private permission: AndroidPermissions) {
      this.platform.registerBackButtonAction(() => {
        console.log('');
      },10000);
      this.id_usuario = navParams.get('id_usuario');
      console.log('iddd',this.id_usuario);

      this.myForm = this.formBuilder.group({
        nombre: ['', Validators.compose([Validators.maxLength(20),Validators.required])],
        apellido: ['', Validators.compose([Validators.maxLength(25),Validators.required])],
        fecha_nac: ['', Validators.compose([ Validators.required])],
        telf: [0, Validators.compose([Validators.maxLength(8), Validators.required])],
        carnet: 0,
        calif_pasa:0
      });
      let info;
      this.mysql.GetUsuario(this.id_usuario).subscribe(
        data => {
          console.log('data', data);
          info= Object.assign(data);
          console.log('exito');


          }, (error: any)=> {
            console.log('error', error);

          }
      );
      this.presentLoading();
      setTimeout(()=>{
        if(info!=undefined)
        {
          this.usuario=info[0];
          this.func();
        }
      },3000);

      this.storage.get('foto').then((val) => {
        this.aux = val;
      });
       
    }
    ionViewDidLoad(){  

      this.fotoUsuario = this.id_usuario;
      this.mysql.validarFotoUsuario(this.fotoUsuario).subscribe(
        data=>{
          console.log(data);  
          if(data['message'] == "existe")
          {
            this.fotoUsuario = "http://ucbtec.net/Aventon/img/Perfil/"+this.fotoUsuario + ".jpg?random+\="+this.aux;
            console.log(this.fotoUsuario);
          }
          if(data['message']=="no existe")
          {
            this.fotoUsuario = "http://ucbtec.net/Aventon/img/defaultUsuario.jpg";
          }
          this.base64Image=this.fotoUsuario;
        },error=>{

        }

      );

    }
    func(){
      this.myForm = this.formBuilder.group({
        nombre: [this.usuario.nombre, Validators.compose([Validators.maxLength(20),Validators.required])],
        apellido: [this.usuario.apellido, Validators.compose([Validators.maxLength(25),Validators.required])],
        fecha_nac: [this.usuario.fecha_nac, Validators.compose([ Validators.required])],
        telf: [ this.usuario.telf, Validators.compose([Validators.maxLength(8), Validators.required])],
        carnet: this.usuario.ci,
        calif_pasa:this.usuario.calif_pasa
      });
    } 
    actualizarPerfil()//funcion para actializar el perfil
      {
        this.usuario.nombre = this.myForm.value.nombre;
        this.usuario.apellido = this.myForm.value.apellido;
        this.usuario.fecha_nac = this.myForm.value.fecha_nac;
        this.usuario.telf = this.myForm.value.telf;
        let info={};
        this.mysql.EditarUser(this.usuario).subscribe(
        data => {
        console.log('data', data);
        info= Object.assign(data);
        console.log('exito');
        }, (error: any)=> {
          console.log('error', error);
        }
    );
    setTimeout(()=>{
        this.mostrarAlerta(); //alerta
        this.navCtrl.setRoot(PasajeroPage,{id_usuario:this.id_usuario}); //redirigir login


    },1000);
    }



    mostrarAlerta() {
      const alert = this.alerta.create({
        title: 'Datos actualizados!',
        subTitle: 'Continue navegando',
        buttons: ['OK']
      });
      alert.present();
    }

  openCamera(){
    this.permission.checkPermission(this.permission.PERMISSION.CAMERA).then(
      result => console.log('tiene permiso?',result.hasPermission),
      err => this.permission.requestPermission(this.permission.PERMISSION.CAMERA)
    );
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation:true
    }

    this.camera.getPicture(options).then((imageData)=> {
    this.base64Image = 'data:image/jpeg;base64,'+ imageData;
    this.fotoUsuario = this.base64Image;
    this.val=true;
  },(err)=>{
    console.log('Error en la foto tomada')
  });

  }

  openGallery(){
    this.permission.checkPermission(this.permission.PERMISSION.CAMERA).then(
      result => console.log('tiene permiso?',result.hasPermission),
      err => this.permission.requestPermission(this.permission.PERMISSION.CAMERA)
    );
    const options: CameraOptions = {
      quality:50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation:true
    }

    this.camera.getPicture(options).then((imageData)=> {
    this.base64Image = 'data:image/jpeg;base64,'+ imageData;
    this.fotoUsuario = this.base64Image;
    this.val=true;
  },(err)=>{
    console.log('Error en la foto tomada')
  });
  }

  uploadingFoto(){
    this.presentLoading2();
    setTimeout(()=>{
      if(this.val== true)
      {
        let url = 'http://ucbtec.net/Aventon/img/Perfil/subirfotosperfil.php';
        let postData = new FormData();
        let nombre = this.usuario.ci;
        postData.append('file',this.base64Image);
        postData.append('nombre',nombre)
        let data: Observable<any> = this.http.post(url,postData);
        data.subscribe((res)=>{
          console.log(res);
        });
        this.storage.set('foto', Math.random());
      }
    },5000);
    
  }

  dismiss(){
    this.navCtrl.setRoot(PasajeroPage,{id_usuario:this.id_usuario}); //redirigir login
  }
  presentLoading() {
    const loader = this.load.create({
      content: "Espere por favor...",
      duration: 3000
    });
    loader.present();
  }

  presentLoading2() {
    const loader = this.load.create({
      content: "Espere por favor...",
      duration: 6000
    });
    loader.present();
  }
}
