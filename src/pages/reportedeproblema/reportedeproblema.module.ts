import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReportedeproblemaPage } from './reportedeproblema';

@NgModule({
  declarations: [
    ReportedeproblemaPage,
  ],
  imports: [
    IonicPageModule.forChild(ReportedeproblemaPage),
  ],
})
export class ReportedeproblemaPageModule {}
