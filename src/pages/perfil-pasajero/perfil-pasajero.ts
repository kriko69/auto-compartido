import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, Platform } from 'ionic-angular';
import { VehiculoPage } from '../vehiculo/vehiculo';
import { Usuario } from '../../interfaces/usuario.interface';

import { ISubscription } from 'rxjs/Subscription';
import { EditarPasajeroPage } from '../editar-pasajero/editar-pasajero';

import {FormGroup, FormBuilder, Validators} from '@angular/forms'; // Para la validacion del formulario
import { Camera, CameraOptions } from '@ionic-native/camera';
import { HttpClient } from '@angular/common/http';
import { Subscription, Observable } from 'rxjs';
import { mysqlService } from '../../services/mysql.service';
import { EditPasswordPage } from '../edit-password/edit-password';
import { OpcionesPasajeroPage } from '../opciones-pasajero/opciones-pasajero';
import { Storage } from "@ionic/storage";
/**
 * Generated class for the PerfilPasajeroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-perfil-pasajero',
  templateUrl: 'perfil-pasajero.html',
})
export class PerfilPasajeroPage {


  id_usuario;
  usuario=[];
  fotoUsuario: string = "";
  val: boolean=false;
  base64Image: string='';
  aux;

  constructor(public navCtrl: NavController, public navParams: NavParams,public app:App,private platform:Platform, public camera:Camera,
    private http: HttpClient,public mysql:mysqlService, public storage:Storage) {
      this.platform.registerBackButtonAction(() => {
      },10000);
      this.id_usuario = navParams.get('id_usuario');
    console.log(this.id_usuario);
    let info;
    this.mysql.GetUsuario(this.id_usuario).subscribe(
      data => {
        console.log('data', data);
        info= Object.assign(data);
        console.log('exito');


        }, (error: any)=> {
          console.log('error', error);

        }
    );

    setTimeout(()=>{ 
      if(info!=undefined)
      {
        this.usuario=info[0];
      }
      console.log(this.usuario);
      
    },1000);

    this.storage.get('foto').then((val) => {
      this.aux = val;
    });
    
  }

  ionViewDidLoad() {
    this.fotoUsuario = this.id_usuario;
      this.mysql.validarFotoUsuario(this.fotoUsuario).subscribe(
        data=>{
          if(data['message']=="existe")
          {
            this.fotoUsuario = "http://ucbtec.net/Aventon/img/Perfil/"+this.fotoUsuario + ".jpg?random+\="+this.aux;
          }
          if(data['message']=="no existe")
          {
            this.fotoUsuario = "http://ucbtec.net/Aventon/img/defaultUsuario.jpg";
          }
          this.base64Image=this.fotoUsuario; 
          ;
        },error=>{
          
        }
        
      );
    
  }
  editPerfil()
  {
    var nav = this.app.getRootNav();
    nav.setRoot(EditarPasajeroPage,{id_usuario: this.id_usuario});//MODIFICADO PARA PASAR LOS PARAMETROS
  }
  editPassword()
  {
//var nav = this.app.getRootNav();
    /*nav.push*/this.navCtrl.setRoot(EditPasswordPage,{id_usuario: this.id_usuario});//MODIFICADO PARA PASAR LOS PARAMETROS

  }
  dismiss(){
        this.navCtrl.setRoot(OpcionesPasajeroPage,{id_usuario: this.id_usuario});//MODIFICADO PARA PASAR LOS PARAMETROS

  }


}
