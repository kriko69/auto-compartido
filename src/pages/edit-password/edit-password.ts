import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, AlertController } from 'ionic-angular';
import { mysqlService } from '../../services/mysql.service';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { ToastService } from '../../services/toast.service';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { PasajeroPage } from '../pasajero/pasajero';
import { PerfilPasajeroPage } from '../perfil-pasajero/perfil-pasajero';
import { OpcionesPasajeroPage } from '../opciones-pasajero/opciones-pasajero';

/**
 * Generated class for the EditPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-password',
  templateUrl: 'edit-password.html',
})
export class EditPasswordPage {
  id_usuario;
  pass: string;
  newpass;
  verpass1='password';
  verpass2='password';
  verpass1_mostrar_ocultar="Ver";
  verpass2_mostrar_ocultar="Ver";
  submitAttempt: boolean = false;
  myForm: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, public platform:Platform,public alerta:AlertController,
    public mysql:mysqlService,public load:LoadingController,public toast:ToastService,private http: HttpClient,
    public formBuilder: FormBuilder, private storage: Storage) {
    this.platform.registerBackButtonAction(() => {
      console.log('');
    },10000);

    this.id_usuario = navParams.get('id_usuario');
  
    this.myForm = this.formBuilder.group({
      passwordd:['',Validators.required],
      pass1: ['', Validators.compose([Validators.pattern(/^[A-Za-z0-9]{6,}$/),Validators.required])]
        }, {'validator': this.compararPassword('passwordd')});

  }


  compararPassword(password:string){
    this.storage.get('password').then((valor) => {
      console.log(valor);
      this.pass = valor;
    });
    return (group: FormGroup): {[key: string]: any} => {
      let passwordd = group.controls[password];

      if (passwordd.value !== this.pass) {
        return {
          compararPassword: true
        };
      }
    }
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad EditPasswordPage');
    console.log(this.id_usuario);
  }
  dismiss(){
    this.navCtrl.setRoot(PerfilPasajeroPage ,{id_usuario:this.id_usuario}); //redirigir login
  }
  mostrarAlerta() {
    const alert = this.alerta.create({
      title: 'Contraseña actualizada!',
      subTitle: 'Continue navegando',
      buttons: ['OK']
    });
    alert.present();
  }

  editarPassword()
  {
        let info={};
        this.mysql.editarPassword(this.id_usuario, this.myForm.value.pass1).subscribe(
        data => {
        console.log('data', data);
        info= Object.assign(data);
        console.log('exito');
        }, (error: any)=> {
          console.log('error', error);
        }
    );
    setTimeout(()=>{
        this.mostrarAlerta(); //alerta
        this.storage.set('password',this.myForm.value.pass1);
        this.navCtrl.setRoot(PerfilPasajeroPage,{id_usuario:this.id_usuario}); //redirigir perfil


    },1000);
  }
  verpass(){
    console.log(this.verpass1);
    
    if(this.verpass1=='password')
    {
      this.verpass1='text';
      this.verpass1_mostrar_ocultar="Ocultar";
    }
    else{
      this.verpass1='password';
      this.verpass1_mostrar_ocultar="Ver";
    }
  }
  verpass22(){
    console.log(this.verpass1);
    
    if(this.verpass2=='password')
    {
      this.verpass2='text';
      this.verpass2_mostrar_ocultar="Ocultar";
    }
    else{
      this.verpass2='password';
      this.verpass2_mostrar_ocultar="Ver";
    }
  }
}
